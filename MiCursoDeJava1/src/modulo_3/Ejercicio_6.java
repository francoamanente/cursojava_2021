package modulo_3;

import java.util.Scanner;

public class Ejercicio_6 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el Curso con un numero entero (de 0 a 12):");
		int curso =scan.nextInt();
		
		if (curso == 0)
			System.out.println("Jardin de Infantes");
		else if (curso >= 1 && curso <= 6)
			System.out.println("Primaria");
		else if (curso >= 7 && curso <= 12)
			System.out.println("Secundaria");
		else
			System.out.println("Error");
		
		scan.close();

	}

}
