package modulo_3;

import java.util.Scanner;

public class Ejercicio_15 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese una Clase de Auto (a,b,c):");
		char claseauto = scan.next().charAt(0);
		
		switch (claseauto)
		{
		case 'a':
			System.out.println("Esta Clase de Auto tiene 4 Ruedas y un Motor");
			break;
		case 'b':
			System.out.println("Esta Clase de Auto tiene 4 Ruedas, un Motor, Cierre Centralizado y Aire");
			break;
		case 'c':
			System.out.println("Esta Clase de autos tiene 4 Ruedas, un Motor, Cierre Centralizado, Aire y Airbag.");
			break;
		default:
			System.out.println("Clase de Auto Inexistente");
			break;
		}
		
		scan.close();
	}

}
