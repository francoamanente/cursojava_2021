package modulo_3;

import java.util.Scanner;

public class Ejercicio_12 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un Numero Entero:");
		int numero = scan.nextInt();
		
		if (numero >= 1 && numero <= 12)
			System.out.println("Pertenece a la Primera Docena");
		else if (numero >= 13 && numero <= 24)
			System.out.println("Pertenece a la Segunda Docena");
		else if (numero >= 25 && numero <= 36)
			System.out.println("Pertenece a la Tercera Docena");
		else
			System.out.println("El Numero " + numero + " esta Fuera de Rango");
		
		scan.close();
	}

}
