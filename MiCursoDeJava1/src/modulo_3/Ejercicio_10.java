package modulo_3;

import java.util.Scanner;

public class Ejercicio_10 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la Primera Variable:");
		int primeravariable =scan.nextInt();
		System.out.println("Ingrese la Segunda Variable:");
		int segundavariable =scan.nextInt();
		System.out.println("Ingrese la Tercera Variable:");
		int terceravariable =scan.nextInt();
		
		if (primeravariable > segundavariable && primeravariable > terceravariable)
			System.out.println("La Primera Variable es la Mayor");
		else if (segundavariable > primeravariable && segundavariable > terceravariable)
			System.out.println("La Segunda Variable es la Mayor");
		else if (terceravariable > primeravariable && terceravariable > segundavariable)
			System.out.println("La Tercera Variable es la Mayor");
		else
			System.out.println("No hay una Variable Mayor");
		
		scan.close();
	}

}
