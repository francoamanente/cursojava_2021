package modulo_3;

import java.util.Scanner;

public class Ejercicio_18 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el Numero que quiere observar en la Tabla de Multiplicar:");
		int numero = scan.nextInt();
		
		for(int j = 0; j < numero + 1; j++ ) {
			System.out.println("La Tabla de Multiplicar a continuacion es la del: " + j);
			for(int i = 0; i <= 10; i++ ) {
				int resultado = j * i;
				System.out.println(j + " x " + i + " = " + resultado);
			}
		}
		
		scan.close();
	}

}
