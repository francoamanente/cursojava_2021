package modulo_3;

import java.util.Scanner;

public class Ejercicio_11 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese una Letra:");
		char letra = scan.next().charAt(0);
		
		if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u' || letra == 'A' || letra == 'E' || letra == 'I' || letra == 'O' || letra == 'U')
			System.out.println("La Letra es una Vocal");
		else
			System.out.println("La Letra NO es una Vocal");
		
		scan.close();
	}

}
