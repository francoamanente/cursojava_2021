package modulo_3;

import java.util.Scanner;

public class Ejercicio_2 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un Numero Entero:");
		int numero = scan.nextInt();
		
		if (numero%2==0) {
			System.out.println("El Numero " + numero + " es Par");
		}
		else {
			System.out.println("El Numero " + numero + " es Impar");
		}
		
		scan.close();

	}

}
