package modulo_3;

import java.util.Scanner;

public class Ejercicio_9 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Para la competicion, los valores a utilizar son los siguientes:\nPiedra = 0\nPapel  = 1\nTijera = 2");
		System.out.println("Ingrese el Valor del Primer Competidor:");
		float primercompetidor=scan.nextFloat();
		System.out.println("Ingrese el Valor del Segundo Competidor:");
		float segundocompetidor=scan.nextFloat();
		
		if (primercompetidor == segundocompetidor)
			System.out.println("Empate");
		else
			if (primercompetidor == 0 && segundocompetidor == 2)
				System.out.println("Gana el Primer Competidor");
			else if (primercompetidor == 0 && segundocompetidor == 1)
				System.out.println("Gana el Segundo Competidor");
			else if (primercompetidor == 1 && segundocompetidor == 0)
				System.out.println("Gana el Primer Competidor");
			else if (primercompetidor == 1 && segundocompetidor == 2)
				System.out.println("Gana el Segundo Competidor");
			else if (primercompetidor == 2 && segundocompetidor == 1)
				System.out.println("Gana el Primer Competidor");
			else if (primercompetidor == 2 && segundocompetidor == 0)
				System.out.println("Gana el Segundo Competidor");

		scan.close();
	}

}
