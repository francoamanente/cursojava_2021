package modulo_3;

import java.util.Scanner;

public class Ejercicio_7 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la Primera Variable:");
		int primeravariable =scan.nextInt();
		System.out.println("Ingrese la Segunda Variable:");
		int segundavariable =scan.nextInt();
		System.out.println("Ingrese la Tercera Variable:");
		int terceravariable =scan.nextInt();
		
		if (primeravariable > segundavariable)
			if (primeravariable > terceravariable)
				System.out.println("La Primera Variable es la Mayor");
			else
				if (primeravariable < terceravariable)
					System.out.println("La Tercera Variable es la Mayor");
				else
					System.out.println("La Primera y la Tercera Variable son las Mayores");
		else
			if (primeravariable < segundavariable)
				if (segundavariable > terceravariable)
					System.out.println("La Segunda Variable es la Mayor");
				else
					if (segundavariable < terceravariable)
						System.out.println("La Tercera Variable es la Mayor");
					else
						System.out.println("La Segunda y la Tercera Variable son las Mayores");
		else
			if (primeravariable == segundavariable)
				if (primeravariable > terceravariable)
					System.out.println("La Primera y la Segunda Variable son las Mayores");
				else
					if (primeravariable < terceravariable)
						System.out.println("La Tercera Variable es la Mayor");
					else
						System.out.println("Todas las Variables son Iguales");
		
		scan.close();

	}

}
