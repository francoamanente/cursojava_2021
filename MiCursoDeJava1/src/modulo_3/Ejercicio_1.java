package modulo_3;

import java.util.Scanner;

public class Ejercicio_1 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingrese la Primera Nota:");
		float nota1 = scan.nextFloat();
		
		System.out.println("Ingrese la Segunda Nota:");
		float nota2 = scan.nextFloat();
		
		System.out.println("Ingrese la Tercera Nota:");
		float nota3 = scan.nextFloat();

		float promedio = (nota1+nota2+nota3)/3;
		
		if (promedio >= 7) {
			System.out.println("Aprobo con un promedio de: " + promedio);
		} 
		
		else {
			System.out.println("Desaprobo con un promedio de: " + promedio);
		}
	
		scan.close();

	}

}
