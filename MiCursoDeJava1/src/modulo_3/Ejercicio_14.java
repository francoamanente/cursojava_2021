package modulo_3;

import java.util.Scanner;

public class Ejercicio_14 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el Puesto conseguido con numeros enteros:");
		int puesto =scan.nextInt();
		
		switch (puesto) {
		case 1:
			System.out.println("Obtiene la Medalla de Oro");
			break;
		case 2:
			System.out.println("Obtiene la Medalla de Plata");
			break;
		case 3:
			System.out.println("Obtiene la Medalla de Bronce");
			break;
		default:
			System.out.println("No obtiene nada, Siga Participando!");
			break;
		}
		
		scan.close();
	}

}
