package modulo_3;

import java.util.Scanner;

public class Ejercicio_13 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un Mes con numeros enteros:");
		int mes = scan.nextInt();
		
		switch (mes) {
		case 2: 
			System.out.println("El Mes ingresado tiene 28 dias");
			break;
		case 4: case 6: case 9: case 11:
			System.out.println("El Mes ingresado tiene 30 dias");
			break;
		case 1: case 3: case 5: case 7: case 8: case 10: case 12:
			System.out.println("El Mes ingresado tiene 31 dias");
			break;
		default:
			System.out.println("No ingreso un Mes correcto");
			break;
		}
		
		scan.close();
	}

}
