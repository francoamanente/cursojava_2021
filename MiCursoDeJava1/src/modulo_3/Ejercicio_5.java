package modulo_3;

import java.util.Scanner;

public class Ejercicio_5 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el Puesto conseguido con numeros enteros:");
		int puesto =scan.nextInt();
		
		if (puesto == 1)
			System.out.println("Obtiene la Medalla de Oro");
		else if (puesto == 2)
			System.out.println("Obtiene la Medalla de Plata");
		else if (puesto == 3)
			System.out.println("Obtiene la Medalla de Bronce");
		else
			System.out.println("No obtiene nada, Siga Participando!");
		
		scan.close();

	}

}
