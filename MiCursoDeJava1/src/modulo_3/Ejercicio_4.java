package modulo_3;

import java.util.Scanner;

public class Ejercicio_4 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese una Categoria (a,b,c):");
		String categoria = scan.nextLine();
		
		if (categoria.equals("a"))
			System.out.println("Hijo");
		if (categoria.equals("b"))
			System.out.println("Padres");
		if (categoria.equals("c"))
			System.out.println("Abuelos");
		
		scan.close();

	}

}
