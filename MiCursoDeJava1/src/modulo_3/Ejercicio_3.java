package modulo_3;

import java.util.Scanner;

public class Ejercicio_3 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un Mes en minusculas:");
		String mes = scan.nextLine();
		
		if (mes.equals("abril") || mes.equals("junio") || mes.equals("septiembre") || mes.equals("noviembre"))
			System.out.println("El Mes ingresado tiene 30 dias");
		else if (mes.equals("enero") || mes.equals("marzo") || mes.equals("julio") || mes.equals("agosto") || mes.equals("octubre") || mes.equals("diciembre"))
			System.out.println("El Mes ingresado tiene 31 dias");
		else if (mes.equals("febrero"))
			System.out.println("El Mes ingresado tiene 28 dias");
		else
			System.out.println("No ingreso un Mes correcto");
		
		scan.close();

	}

}
