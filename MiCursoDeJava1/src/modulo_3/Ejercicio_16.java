package modulo_3;

import java.util.Scanner;

public class Ejercicio_16 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el Numero que quiere observar en la Tabla de Multiplicar:");
		int numero = scan.nextInt();
		
		System.out.println("La Tabla de Multiplicar a continuacion es la del: " + numero);
		
		for (int i = 0; i <= 10; i++ ) {
			int resultado = numero * i;
			System.out.println(numero + " x " + i + " = " + resultado);
		}
		
		scan.close();
	}

}
