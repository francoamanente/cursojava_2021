package modulo_3;

public class Ejercicio_19 {

	public static void main(String[] args) {

		int cantnum = 0;
		int suma = 0;
		int prom = 0;
			
		while (cantnum <= 10) {
			int numaza = (int) Math.floor(Math.random()*1000);
			System.out.println("El Numero al Azar es: " + numaza);	
			suma = suma + numaza;
			cantnum = cantnum + 1;
		}
		System.out.println("La Suma de los Numeros conseguidos al Azar es de: " + suma);
		
		prom = suma/10;
		
		System.out.println("El Promedio de la Suma anterior es de: " + prom);
	}

}
