package modulo_2;

public class Ejercicio_5 {

	public static void main(String[] args) {
		
		byte  bmax = 127;
	    short smax = 32767;
	    int   imax = 2147483647;
	    long  lmax = 9223372036854775807L;

	    long  l1 = smax;
	    int   i1 = smax; 
		byte  b1 = (byte)  smax; 
		long  l2 = imax;
		byte  b2 = (byte)  imax; 
		short s1 = (short) imax; 
		
		System.out.println("Las siguientes lineas dan errores de compilacion: b=s; b=i; s=i");
		System.out.println("Las siguientes lineas NO dan errores de compilacion: l=s; i=s; l=i");
	}

}
