package modulo_2;

import java.util.Scanner;

public class Ejercicio_3 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingrese el Tipo de Division:");
		char division = scan.next().charAt(0);
		System.out.println("El Tipo de Division es: " + division);
		
		System.out.println("Ingrese la Cantidad de Goles hechos:");
		byte goles = scan.nextByte();
		System.out.println("La Cantidad de Goles hechos fue de: " + goles);
		
		System.out.println("Ingrese la Cantidad de Partidos Jugados:");
		byte partidos = scan.nextByte();
		
		System.out.println("Ingrese la Capacidad de la Cancha:");
		int capacidad = scan.nextInt();
		System.out.println("La Capacidad de la Cancha es de: " + capacidad + " Personas");
		
		float promediogoles = (float) goles/partidos;
		System.out.println("El Promedio de Goles por Partido fue de: "+ promediogoles);
		
		scan.close();

	}

}
