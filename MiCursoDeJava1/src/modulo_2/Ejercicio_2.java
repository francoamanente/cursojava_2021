package modulo_2;

public class Ejercicio_2 {

	public static void main(String[] args) {
		
		byte  bmin = -128;
		byte  bmax = 127;
	    short smin = -32768;
	    short smax = 32767;
	    int   imin = -2147483648;
	    int   imax = 2147483647;
	    long  lmin = -9223372036854775808L;
	    long  lmax = 9223372036854775807L;
	
	    System.out.println("Tipo\t\t\tMinimo\t\t\t\tMaximo");
		System.out.println("----\t\t\t------\t\t\t\t------");
		System.out.println("\nByte\t\t\t " + bmin + "\t\t\t\t " + bmax);
		System.out.println("\nShort\t\t\t" + smin + "\t\t\t\t" + smax);
		System.out.println("\nInt\t\t     " + imin + "\t\t      " + imax);
		System.out.println("\nLong\t\t " + lmin + "\t\t " + lmax);
		System.out.println("\nLa formula que me permite mostrar los minimos teniendo en cuenta la cantidad de bits es: -2*(cantidad de bits)");
		System.out.println("\nLa formula que me permite mostrar los maximos teniendo en cuenta la cantidad de bits es: 2*(cantidad de bits)-1");

	}

}
