package modulo_2;

public class Ejercicio_4 {

	public static void main(String[] args) {
		
		byte b = 10;
		short s = 20;
		int i = 30;
		long l = 40;
		
		byte sumabb = (byte) (b+b);
		byte sumabs = (byte) (b+s);
		byte sumabi = (byte) (b+i);
		byte sumaii = (byte) (i+i);
		byte sumasl = (byte) (s+l);
		
		System.out.println("La Suma b + b da como resultado: " + sumabb);
		System.out.println("La Suma b + s da como resultado: " + sumabs);
		System.out.println("La Suma b + i da como resultado: " + sumabi);
		System.out.println("La Suma i + i da como resultado: " + sumaii);
		System.out.println("La Suma s + l da como resultado: " + sumasl);
		
	}

}
